package com.eauction.buyer.service;

import com.eauction.buyer.entity.BuyerBidDetail;
import com.eauction.buyer.entity.ProductDetail;
import com.eauction.buyer.exception.BidEndedException;
import com.eauction.buyer.exception.DuplicateBidException;
import com.eauction.buyer.repository.BidDetailsRepository;
import com.eauction.buyer.repository.ProductDetailsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BidValidationServiceTest {
    @Mock
    private BidDetailsRepository bidDetailsRepository;
    @Mock
    private ProductDetailsRepository productDetailsRepository;
    @InjectMocks
    private BidValidationService validationService;

    @Test
    void testValidateBidEndDateException() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setBidEndDate("12/12/2021");
        Mockito.when(productDetailsRepository.fetchProductById("1111-2222")).thenReturn(productDetail);
        Exception exception = assertThrows(BidEndedException.class, () -> validationService.validateBidEndDate("1111-2222"));
        assertEquals("com.eauction.buyer.exception.BidEndedException",
                exception.getClass().getName());
        assertEquals("Bid cannot be placed or updated. Product Bidding is ended", exception.getMessage());
    }

    @Test
    void testValidateBidAssociationDuplicateException() {
        String productId = "1111-2222";
        String email = "bravin@gmail.com";
        Mockito.when(bidDetailsRepository.fetchBids(productId ,email)).thenReturn(new BuyerBidDetail());
        Exception exception = assertThrows(DuplicateBidException.class, () -> validationService.validateBidAssociation(productId,email));
        assertEquals("com.eauction.buyer.exception.DuplicateBidException",
                exception.getClass().getName());
        assertEquals("Duplicate Bid. Already a Bid is placed for this product", exception.getMessage());
    }

    @Test
    void testValidateBidEndDate() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setBidEndDate("12/12/2022");
        Mockito.when(productDetailsRepository.fetchProductById("1111-2222")).thenReturn(productDetail);
        assertTrue(validationService.validateBidEndDate("1111-2222"));
    }

    @Test
    void testValidateBidAssociation() {
        String productId = "1111-2222";
        String email = "bravin@gmail.com";
        Mockito.when(bidDetailsRepository.fetchBids(productId ,email)).thenReturn(null);
        assertTrue(validationService.validateBidAssociation(productId,email));
    }


}