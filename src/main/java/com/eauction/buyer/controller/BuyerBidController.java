package com.eauction.buyer.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eauction.buyer.dto.BuyerBidDetailDTO;
import com.eauction.buyer.entity.BuyerBidDetail;
import com.eauction.buyer.exception.BidEndedException;
import com.eauction.buyer.exception.DuplicateBidException;
import com.eauction.buyer.service.BidValidationService;
import com.eauction.buyer.service.BuyerBidService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RestController
@RequiredArgsConstructor
@RequestMapping("/buyer")
@Log4j2
public class BuyerBidController {

	private static final Logger log = LogManager.getLogger(BuyerBidController.class);
	@Autowired
    private  BuyerBidService buyerBidService;
	@Autowired
    private  BidValidationService bidValidationService;

    @PostMapping("/place-bid")
    public ResponseEntity<BuyerBidDetail> placeBid(@Valid @RequestBody BuyerBidDetailDTO buyerBidDetailDTO) {
        bidValidationService.validateBidEndDate(buyerBidDetailDTO.getProductId());
        bidValidationService.validateBidAssociation(buyerBidDetailDTO.getProductId(),buyerBidDetailDTO.getEmail());
        return Optional.ofNullable(buyerBidService.placeBid(buyerBidDetailDTO))
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PatchMapping("/update-bid/{productId}/{buyerEmailId}/{newBidAmount}")
    public ResponseEntity<BuyerBidDetail> updateBid(@PathVariable("productId") String productId, @PathVariable("buyerEmailId") String buyerEmailId,
                                                    @PathVariable("newBidAmount") String newBidAmount) {
        log.debug("Received Inputs: {} {} {}", productId, buyerEmailId,newBidAmount);
        bidValidationService.validateBidEndDate(productId);
        return Optional.ofNullable(buyerBidService.updateBid(productId, buyerEmailId, newBidAmount))
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @ExceptionHandler(DuplicateBidException.class)
    public ResponseEntity<String> handleDuplicateException(DuplicateBidException businessRuleServiceException){
        return new ResponseEntity<>(businessRuleServiceException.getMessage(),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BidEndedException.class)
    public ResponseEntity<String> handleBidEndException(BidEndedException businessRuleServiceException){
        return new ResponseEntity<>(businessRuleServiceException.getMessage(),HttpStatus.BAD_REQUEST);
    }

}