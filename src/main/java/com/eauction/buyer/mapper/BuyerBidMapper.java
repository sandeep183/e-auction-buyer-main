package com.eauction.buyer.mapper;

import com.eauction.buyer.dto.BuyerBidDetailDTO;
import com.eauction.buyer.entity.BuyerBidDetail;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BuyerBidMapper {

    BuyerBidDetail fromProductDetailsDTO(BuyerBidDetailDTO buyerBidDetailDTO);

}