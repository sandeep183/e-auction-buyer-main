package com.eauction.buyer.service;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.eauction.buyer.dto.BuyerBidDetailDTO;
import com.eauction.buyer.entity.BuyerBidDetail;
import com.eauction.buyer.entity.BuyerBidUpdate;
import com.eauction.buyer.mapper.BuyerBidMapper;
import com.eauction.buyer.repository.BidDetailsRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.utils.StringUtils;

@Service
@RequiredArgsConstructor
@Log4j2
public class BuyerBidService {

	private static final Logger log = LogManager.getLogger(BuyerBidService.class);
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    private final String TOPIC = "bid-topic";
    private final String UPDATE_TOPIC = "bid-update-topic";
    @Autowired
    private  BidDetailsRepository bidDetailsRepository;
    @Autowired
    private  BuyerBidMapper buyerBidMapper;

    public BuyerBidDetail placeBid(BuyerBidDetailDTO buyerBidDetailDTO) {
        log.debug("Received input: {}", buyerBidDetailDTO);
        if(Objects.nonNull(buyerBidDetailDTO)) {
            BuyerBidDetail buyerBidDetail = buyerBidMapper.fromProductDetailsDTO(buyerBidDetailDTO);
            bidDetailsRepository.placeBid(buyerBidDetail);
            try {
                this.kafkaTemplate.send(TOPIC, buyerBidDetail);
                log.debug("Send to kafka");
            } catch(Exception exception) {
                log.error("Exception occurred while posting message to queue"+ exception.getMessage());
            }
            return buyerBidDetail;
        }
        return null;
    }

    public BuyerBidDetail updateBid(String productId, String emailId, String bidAmount) {
        log.debug("Received inputs: {} {} {}", productId, emailId, bidAmount);

        if(!StringUtils.isEmpty(productId) && !StringUtils.isEmpty(emailId) && !StringUtils.isEmpty(bidAmount)) {
            BuyerBidUpdate buyerBidUpdate = new BuyerBidUpdate();
            buyerBidUpdate.setProductId(productId);
            buyerBidUpdate.setEmail(emailId);
            buyerBidUpdate.setBidAmount(bidAmount);
            try {
                this.kafkaTemplate.send(UPDATE_TOPIC, buyerBidUpdate);
                log.debug("Send update to kafka");
            } catch(Exception exception) {
                log.error("Exception occurred while posting update message to queue"+ exception.getMessage());
            }
          return bidDetailsRepository.updateBid(productId,emailId,bidAmount);
          //  return productId;
        }
        return null;
    }

}