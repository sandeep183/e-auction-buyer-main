package com.eauction.buyer.repository;

import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primaryPartitionKey;
import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primarySortKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eauction.buyer.entity.BuyerBidDetail;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;

@Repository
@RequiredArgsConstructor
@Log4j2
public class BidDetailsRepository {
	@Autowired
    private  DynamoDbEnhancedClient dynamoDbEnhancedClient;

    private static final TableSchema<BuyerBidDetail> BID_DETAILS_TABLE_SCHEMA =
            TableSchema.builder(BuyerBidDetail.class)
                    .newItemSupplier(BuyerBidDetail::new)
                    .addAttribute(String.class, a -> a.name("productId")
                            .getter(BuyerBidDetail::getProductId)
                            .setter(BuyerBidDetail::setProductId)
                            .tags(primaryPartitionKey()))
                    .addAttribute(String.class, a -> a.name("bidAmount")
                            .getter(BuyerBidDetail::getBidAmount)
                            .setter(BuyerBidDetail::setBidAmount))
                    .addAttribute(String.class, a -> a.name("firstName")
                            .getter(BuyerBidDetail::getFirstName)
                            .setter(BuyerBidDetail::setFirstName))
                    .addAttribute(String.class, a -> a.name("lastName")
                            .getter(BuyerBidDetail::getLastName)
                            .setter(BuyerBidDetail::setLastName))
                    .addAttribute(String.class, a -> a.name("address")
                            .getter(BuyerBidDetail::getAddress)
                            .setter(BuyerBidDetail::setAddress))
                    .addAttribute(String.class, a -> a.name("city")
                            .getter(BuyerBidDetail::getCity)
                            .setter(BuyerBidDetail::setCity))
                    .addAttribute(String.class, a -> a.name("state")
                            .getter(BuyerBidDetail::getState)
                            .setter(BuyerBidDetail::setState))
                    .addAttribute(String.class, a -> a.name("pin")
                            .getter(BuyerBidDetail::getPin)
                            .setter(BuyerBidDetail::setPin))
                    .addAttribute(String.class, a -> a.name("phone")
                            .getter(BuyerBidDetail::getPhone)
                            .setter(BuyerBidDetail::setPhone))
                    .addAttribute(String.class, a -> a.name("email")
                            .getter(BuyerBidDetail::getEmail)
                            .setter(BuyerBidDetail::setEmail)
                    .tags(primarySortKey()))
                    .build();


    public void placeBid(BuyerBidDetail buyerBidDetail) {
        final DynamoDbTable<BuyerBidDetail> bidDetailsTable = dynamoDbEnhancedClient.table("bid-details",
                BID_DETAILS_TABLE_SCHEMA);
        bidDetailsTable.putItem(buyerBidDetail);
    }

    public BuyerBidDetail fetchBids(String productId, String email) {
        final DynamoDbTable<BuyerBidDetail> productDetailsTable = dynamoDbEnhancedClient.table("bid-details",
                BID_DETAILS_TABLE_SCHEMA);
        return productDetailsTable.getItem(Key.builder().partitionValue(productId).sortValue(email).build());
    }

    public BuyerBidDetail updateBid(String productId, String email, String amount) {
        final DynamoDbTable<BuyerBidDetail> bidDetailsTable = dynamoDbEnhancedClient.table("bid-details",
                BID_DETAILS_TABLE_SCHEMA);
        BuyerBidDetail bidDetails = bidDetailsTable.getItem(Key.builder().partitionValue(productId).sortValue(email).build());
        bidDetails.setBidAmount(amount);
        return bidDetailsTable.updateItem(bidDetails);
    }
}
