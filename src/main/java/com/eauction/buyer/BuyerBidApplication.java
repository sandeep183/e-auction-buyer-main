package com.eauction.buyer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class BuyerBidApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyerBidApplication.class, args);
	}

}
