package com.eauction.buyer.exception;

public class DuplicateBidException extends RuntimeException {
    public DuplicateBidException(String message) {
        super(message);
    }
}