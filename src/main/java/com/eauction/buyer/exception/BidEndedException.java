package com.eauction.buyer.exception;

public class BidEndedException extends RuntimeException{
    public BidEndedException(String message) {
        super(message);
    }
}